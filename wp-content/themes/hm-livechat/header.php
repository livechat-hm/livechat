<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hm-livechat
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
<div id="page" class="site">

	<header id="masthead" class="site-header">

		<div class="container">

			<div class="row">
				
				<div class="col">

					<h1><?php bloginfo('name'); ?></h1>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-hm.png" id="logo" alt="Harmonie Mutuelle - Groupe Vyv">

				</div>

			</div>

		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
