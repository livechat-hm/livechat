(function($) {
	var triggerBttn = document.getElementById( 'trigger-overlay' ),
		overlay = document.querySelector( 'div.overlay' ),
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {
		
		if( $(overlay).hasClass( 'open' ) ) {
			$(overlay).removeClass( 'open' );
			$(overlay).addClass( 'close' );
			$(triggerBttn).removeClass( 'is-active' );
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				$(overlay).removeClass ( 'close' );
			};
			if( support.transitions ) {
				overlay.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
		}
		else if( !$(overlay).hasClass( 'close' ) ) {
			$(triggerBttn).addClass( 'is-active' );
			$(overlay).addClass( 'open' );
		}
	}

	triggerBttn.addEventListener( 'click', toggleOverlay );
})( jQuery );