(function($) {


	function intro() {

		var $window = $(window);

		$('.carousel').slick({
			autoplay: 		true,
			autoplaySpeed: 	6000,
			arrows: 		false,
			dots: 			true
		});

		$('.intervenants-carousel').slick({
			autoplay: 		true,
			autoplaySpeed: 	6000,
			arrows: 		false,
			dots: 			true
		});

		$('.sidebar').theiaStickySidebar();

		// Gestion du compteur

		var date_array = $('#date').html().split(", ");

		var startDate = new Date (date_array[0], date_array[1] - 1, date_array[2], date_array[3], date_array[4], date_array[5]);


		// On masque les questions si le livechat est dans moins de 1 jour...

		var t2 = new Date();
		var dif = t2.getTime() - startDate.getTime();
		var t = Math.abs(dif);
		var days = Math.floor( t/(1000*60*60*24) );

		if (days < 1) {
			$('.question').remove();
		}

		function formatNumber (n) {
			var formattedNumber = ("0" + n).slice(-2);
			return formattedNumber;
		}

		function get_uptime() {

		    var t2 = new Date();
		    var dif = t2.getTime() - startDate.getTime();

		    if (dif < 0) {

		    	var t = Math.abs(dif);

		    	var seconds = formatNumber( Math.floor( (t/1000) % 60 ) );
				var minutes = formatNumber( Math.floor( (t/1000/60) % 60 ) );
				var hours = formatNumber( Math.floor( (t/(1000*60*60)) % 24 ) );
				var days = formatNumber( Math.floor( t/(1000*60*60*24) ) );

				$('#jours').html(days);
				$('#heures').html(hours);
				$('#minutes').html(minutes);
				$('#secondes').html(seconds);

		    	setTimeout(get_uptime, 1000);
		    } else {
		    	console.log('On lance le chat');
		    	$('.compteur .before').addClass('d-none');
		    	$('.compteur .during').removeClass('d-none');
		    }
		}

		get_uptime();

	}

	if ( $('body').hasClass('single-livechat') && !$('body').hasClass('session') ) {
		intro();
	}



})( jQuery );