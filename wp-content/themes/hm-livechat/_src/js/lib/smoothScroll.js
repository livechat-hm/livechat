
;(function (window, document, $, undefined) {
    'use strict';

    if ( !$ ) {
        return;
    }

    var headerHeight = 65;

    $.fn.smoothTo = function(target, emetteur) {


      var target = $(target);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        //event.preventDefault();

        var offset = target.offset().top;

        if($(emetteur).data('smooth-offset')) {
          offset -= $(emetteur).data('smooth-offset');
        } else if (target.attr('id') !== 'page') {
          offset -= headerHeight;
        }
        $('html, body').animate({
          scrollTop: offset
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          /*var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };*/
        });
      }
    };

    $('[data-smooth]')
      .click(function(event) {
        event.preventDefault();
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          
          $(this).smoothTo(this.hash, this);
          
        }
      });

}( window, document, window.jQuery || jQuery ));