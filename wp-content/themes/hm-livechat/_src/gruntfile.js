module.exports = function(grunt) {

	'use strict';

	// Load all grunt packages
	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					style: 'compressed',
					sourcemap: 'auto'
				},
				files: {
						'../css/theme.css': [
							'scss/main.scss'
						]
				}
			}
		},
		postcss: {
		    options: {
		      map: true,

		      processors: [
		        require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
		      ]
		    },
		    dist: {
		      src: '../css/theme.css'
		    }
		},

		uglify: {
			options: {
				sourceMap: true,
			    sourceMapIncludeSources : true,
			    sourceMapIn : 'js/compil/custom.js.map'
			},
			targets: {
				files: {
					'../js/main.js' : 'js/compil/custom.js',
				} //Files
			}
		},
		concat: {
			options: {
				sourceMap: true
			},
			targets: {
				files: {
					'js/compil/custom.js' : [
						'js/lib/popper.js',
						'js/lib/bootstrap.js',
						'js/lib/star-rating.js',
						'js/lib/image-picker.min.js',
						//'js/lib/css3-animate-it.js',
						'js/lib/slick.js',
						'js/lib/theia-sticky-sidebar.js',
						//'js/lib/scrollmagic/ScrollMagic.js',
						//'js/lib/scrollmagic/plugins/debug.addIndicators.js',
						//'js/lib/scrollmagic/plugins/animation.gsap.js',
						//'js/lib/ResizeSensor.js',
						//'js/lib/theia-sticky-sidebar.js',
						//'js/lib/typed.js',
						//'js/lib/jarallax.js',
						//'js/lib/smoothScroll.js',
						//'js/burger.js',
						//'js/lib/bootstrap-select.js',
						'js/main.js'],
				} //Files
			}
		}, //concat

		modernizr: {
		  dist: {
		    "parseFiles": true,
		    "customTests": [],
		    "devFile": "js/lib/modernizr.js",
		    "dest": "js/lib/modernizr.js",
		    "tests": [
			  ],
			  "options": [
			    "prefixed",
			    "setClasses"
			  ],
		  }
		},

		watch : {
			options: {
				livereload: true
			},
			html: {
				files: ['../template-parts/*.php', '../inc/*.php', '../*.php'],
			},
			scripts: {
				files: ['js/**/*.js', 'js/*.js'],
			    tasks: ['concat', 'uglify']
			}, //scripts
			notask: {
				files: ['../js/session.js', '../js/session-vue.js', '../js/session-model.js'],
			    tasks: []
			}, //scripts
			sass: {
				files: ['scss/*.scss', 'scss/**/*.scss'],
		   		tasks: ['sass', 'postcss' ]
			}
		}
	}); //initConfig
	grunt.registerTask('default', ['watch']);
	//grunt.registerTask('init', ['copy_mate', 'concat', 'uglify', 'sass', 'cssmin']);
	//grunt.registerTask('build', ['concat', 'uglify', 'sass', 'cssmin']);
} //exports
