


var lcModel;

(function($) {

	firstBy=(function(){function e(f){f.thenBy=t;return f}function t(y,x){x=this;return e(function(a,b){return x(a,b)||y(a,b)})}return e})();

	Vue.component('star-rating', VueStarRating.default);

	lcModel = new Vue({

		el: '#chatroom',
		
		data: {
	  		current_user: false,
	  		users: [],
	  		chargement: true,
	  		fil: [],
	  		openTime: '',
	  		intervalTime: '',
	  		time: '0',
	  		modalQuestion: {},
	  		reponse: ''
		},

		computed: {
			connection_needed: function() {
				if (!this.current_user.conn && !this.chargement) {
					return true;
				} else {
					return false;
				}
			},
			
			users_compt: function () {
				var compte = 0;

				//console.log('/////////////////////');

				$.each(this.users, function( index, value ) {

				  if (value.conn) {
				  	//console.log (value);
				  	compte++;
				  }
				});

				return compte;

		    },
		    nouvelles_questions: function () {
		    	var nouvelles = [];
		    	this.fil.forEach(function(v) {
		    		if (v.status === STATUS_MESSAGE.NOUVELLES && v.type === 0) {
		    			nouvelles.push(v);
		    		}
		    	});
		    	nouvelles.sort(
				    firstBy(function (v1, v2) { return v2.note - v1.note; })
				    .thenBy(function (v1, v2) { return v1.date > v2.date; })
				);
		    	return nouvelles;
		    },
		    publication: function () {
		    	//console.log(this.fil);
		    	var publication = [];
		    	this.fil.forEach(function(v) {
		    		if ( (v.status === STATUS_MESSAGE.PUBLIE) || (v.status === STATUS_MESSAGE.REPONDU) ) {
		    			publication.push(v);
		    		}
		    	});
		    	publication.sort(this.orderByPublicationDate);
		    	//this.animateMessagesToBottom();
		    	return publication;
		    },

		    refusees_questions: function () {
		    	var refusees = [];
		    	this.fil.forEach(function(v) {
		    		if (v.status === STATUS_MESSAGE.REFUSEES && v.type === 0) {
		    			refusees.push(v);
		    		}
		    	});
		    	refusees.sort(
				    firstBy(function (v1, v2) { return v2.note - v1.note; })
				    .thenBy(function (v1, v2) { return v1.date > v2.date; })
				);
		    	return refusees;
		    },
		    a_venir_questions: function () {
		    	var a_venir = [];
		    	this.fil.forEach(function(v) {
		    		if ( (v.status === STATUS_MESSAGE.A_REDIGER || v.status === STATUS_MESSAGE.A_PUBLIER) && ( v.type === 0 ) ) {
		    			a_venir.push(v);
		    		}
		    	});
		    	a_venir.sort(
				    firstBy(function (v1, v2) { return v2.note - v1.note; })
				    .thenBy(function (v1, v2) { return v1.date > v2.date; })
				);
		    	return a_venir;
		    },
		    a_publier_questions: function () {
		    	var a_publier = [];
		    	this.fil.forEach(function(v) {
		    		if ( v.status === STATUS_MESSAGE.A_PUBLIER && v.type === 0 ) {
		    			a_publier.push(v);
		    		}
		    	});
		    	a_publier.sort(
				    firstBy(function (v1, v2) { return v2.note - v1.note; })
				    .thenBy(function (v1, v2) { return v1.date > v2.date; })
				);
		    	return a_publier;
		    },
		    rediger_questions: function () {
		    	var rediger = [];
		    	this.fil.forEach(function(v) {
		    		if ( v.status === STATUS_MESSAGE.A_REDIGER && v.type === 0 ) {
		    			rediger.push(v);
		    		}
		    	});
		    	rediger.sort(
				    firstBy(function (v1, v2) { return v2.note - v1.note; })
				    .thenBy(function (v1, v2) { return v1.date > v2.date; })
				);
		    	return rediger;
		    }
		},

	    beforeDestroy () {
	       clearInterval(this.intervalTime);
	    },

		methods: {

			// UTILS
			orderByPublicationDate: function(a, b) {

				// -> Date sera à remplacer par publication

				var aName = a.date;
				var bName = b.date; 
				return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
			},

			checkLike: function(msg_id) {
				var msg = this.getMessageById (msg_id );
				var like_array = msg.like;
				var current_user_id = this.current_user.id;

				var retour = false;

				like_array.forEach(function(element) {
					if (current_user_id == element) {
						retour = true;
					}
				});

				return retour;
			},

			animateMessagesToBottom: function() {
		    	var height = $('.publication-container .inner').height();
		    	var $t = $('.publication-container');
    			$t.animate( {"scrollTop": height }, "slow");
		    },

			// METHODS
			setUsers: function(users) {
				this.users = users;
			},
		  	setCurrentUser: function (user_id) {
				this.current_user = this.getUserById(user_id);
			},
			getUserById: function (id) {
				var result = $.grep(this.users, function(e){ return e.id == id; });
				return result[0];
			},
			updateUser: function (user) {

				var user_id = undefined;

				$.each(this.users, function( index, value ) {
				  if (value.id === user.id) {
				  	user_id = index;
				  }
				});

				if (user_id !== undefined) {
					Vue.set(this.users, user_id, user);
				} else {
					this.users.push ( user );
				}

			},
			setFil: function(fil) {
				this.fil = fil;
				//this.animateMessagesToBottom();
			},
			setOpenTime: function(livechatinit) {
				if (livechatinit) {
					this.openTime = new Date( parseFloat(livechatinit) );
					if (!this.intervalTime) {
						this.elapsedTime();
					}
				} else {
					this.openTime = '';
				}
			},
			updateMessage: function(msg) {

				if (msg) {

					var message_id = undefined;

					$.each(this.fil, function( index, value ) {
					  if (value.id === msg.id) {
					  	message_id = index;
					  	return;
					  }
					});

					if (message_id !== undefined) {
						Vue.set(this.fil, message_id, msg);
					} else {
						this.addMessage(msg);
					}

				}

			},
			saveCookie: function(){

				var current = lcModel.current_user;

				jQuery.post(

				    data.ajaxurl,
				    {
				        'action': 	'load_chat',
				        'uid': 		current.id, 
				        'pseudo': 	current.name, 
				        'avatar': 	current.avatar, 
				        'status': 	current.status, 
				    },
				    function(response){
				    	
					}
				);

			},
			getMessageById: function (id) {
				var result = $.grep(this.fil, function(e){ return e.id == id; });
				return result[0];
			},
			getUsersCompt: function() {

				var compte = 0;

				$.each(this.users, function( index, value ) {
				  if (value.conn) {
				  	compte++;
				  }
				});

				return compte;
			},
			addMessage: function(msg) {
				this.fil.push(msg);
			},
			refuse: function(msg_id) {

				var msg = jQuery.extend({}, this.getMessageById(msg_id));
				msg.status = STATUS_MESSAGE.REFUSEES;
				livechat.updateQuest(msg);
			},
			restaurer: function(msg_id) {

				var msg = jQuery.extend({}, this.getMessageById(msg_id));
				msg.status = STATUS_MESSAGE.NOUVELLES;
				livechat.updateQuest(msg);
			},
			getQuestionsFil: function() {

				var questionsNouvelles = [];

				$.each(this.fil, function( index, value ) {
					if ( value.status == STATUS_MESSAGE.NOUVELLES ) {
						questionsNouvelles.push ( value );
					}
				});

				return questionsNouvelles;
			},
			elapsedTime: function(){           
			    this.intervalTime = setInterval(function(){
			        var delayTime = new Date() - this.openTime;
			        var sec_num = Math.floor((delayTime / 1000));

				    //var hours   = Math.floor(sec_num / 3600);
				    var minutes = Math.floor((sec_num) / 60);
				    var seconds = sec_num - (minutes * 60);

				    //if (hours   < 10) {hours   = "0"+hours;}
				    //if (minutes < 10) {minutes = "0"+minutes;}
				    if (seconds < 10) {seconds = "0"+seconds;}
			        this.time = minutes;

			        //console.log (this.openTime);
			    }.bind(this), 5000);
			},
			addLike: function(v) {
				livechat.addLike ( v );
			},
			setRating: function(rate, msg_id) {
				var msg = jQuery.extend({}, this.getMessageById(msg_id));
				msg.note = rate;
				setTimeout(function () {
			        livechat.updateQuest(msg);
			    }, 500);
				
			},
			openchat: function() {
				this.elapsedTime();
				livechat.openchat();
			},
			closechat: function() {
				clearInterval(this.intervalTime);
				livechat.closechat();
			},
			connectForm: function() {

				var pseudo = $('#connect-form #pseudo').val();
				var avatar = $('#connect-form #avatar').val();

				jQuery.post(

				    data.ajaxurl,
				    {
				        'action': 	'check_user_status'
				    },
				    function(response){

				    	var status = response;
				    	livechat.connect_user( status, pseudo, avatar );
					}
				);
				//var status = $('#connect-form #status').val();

				

			},
			sendMessage: function() {
				console.log('sendMessage');
				if ($('#chat-zone').val() != '') {
					livechat.sendMessage($('#chat-zone').val());
					$('#chat-zone').val('');
				}
			},
			validerQuest: function(msg_id) {
				var msg = jQuery.extend({}, this.getMessageById(msg_id));
				if (msg.status == STATUS_MESSAGE.NOUVELLES) {
					msg.status = STATUS_MESSAGE.A_REDIGER;
					livechat.updateQuest(msg);
				} else if (msg.status == STATUS_MESSAGE.A_PUBLIER) {
					msg.status = STATUS_MESSAGE.PUBLIE;
					livechat.publishQuest(msg);
				}
				
			},
			publierReponse: function(rep_id, msg) {
				var reponse = jQuery.extend({}, this.getMessageById(rep_id));
				reponse.status = STATUS_MESSAGE.PUBLIE;
				livechat.publishQuest(reponse);

				msg.status = STATUS_MESSAGE.REPONDU;
				livechat.updateQuest (msg);
				
			},
			editQuestion: function(msg) {
				this.modalQuestion = JSON.parse(JSON.stringify(msg));
			},
			editQuest: function(e) {
				livechat.updateQuest(this.modalQuestion);
				$('#edit-question').modal('hide');
			},
			editRep: function() {

				if ( this.reponse != '' ) {
					if (this.modalQuestion.repid) {
						var reponse = this.getMessageById (this.modalQuestion.repid);
						reponse.question = this.reponse;
						livechat.updateQuest( reponse );
					} else {
						livechat.sendMessage(this.reponse, TYPE_MESSAGE.REPONSE, this.modalQuestion.id);
					}
					$('#edit-reponse').modal('hide');				
				}

			},
			editReponse: function(msg) {

				if ( msg.repid ) {
					var question = this.getMessageById (msg.repid);
					this.reponse = question.question;
				} else {
					this.reponse = '';
				}
				
				this.modalQuestion = JSON.parse(JSON.stringify(msg));
			}
		},

		filters: {
			getAvatarByUser: function(user_id) {

				var user = lcModel.getUserById (user_id); 

				//console.log ('Avatar: ' + user.avatar);

				return data.avatars[user.avatar];

				//return data.themeurl + '/img/avatars/' + user.avatar + '.jpg';
			},

			getNom: function(user_id) {
				var user = lcModel.getUserById (user_id); 

				return user.name;

			},

			printHour: function (h) {
				var heure = new Date(h);
				return heure.getHours() + 'h' + ('0'+heure.getMinutes()).slice(-2);
			},

			messagePrint: function (text) {
				//return 'hello';
				return text.replace("\r\n", "\\n");
				//return text.replace(/\n/g, "<br>") + 'oh no';
			}
		},

	    watch: {
	        connection_needed(newValue){
	        	
	        	if (newValue) {
	        		Vue.nextTick(function() {
					  $('#avatar').imagepicker();
					});
	        		
	        	}
	        }
	    }

	});


})( jQuery );

