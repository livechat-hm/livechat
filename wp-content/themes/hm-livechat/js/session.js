

var livechat, socket;

(function($) {

	socket = io.connect(URL_SOCKET);

	livechat = {

		checkCookie: function(){

			jQuery.post(
			    data.ajaxurl,
			    {
			        'action': 	'check_cookie'
			    },
			    function(response){

			    	if (!response) {
			    		lcModel.chargement = false;
			    	} else {

			    		livechat.reconnect_user (response.status, response.pseudo, response.avatar, response.uid);
			    	}

		        }
			);
		}, 		

		connect_user: function (status, pseudo, avatar) {

			socket.emit('setuserprop', status, pseudo, avatar );
			//lcVue.showLoader();
		},

		reconnect_user: function (status, pseudo, avatar, uid) {

			socket.emit('setuserprop', status, pseudo, avatar, uid );
			//lcVue.showLoader();
		},

		setuserpropret: function(userid, users, fil) {			

			//console.log("Fil");
			//console.log(fil);

			console.log('One time0');

			console.log(users);

			console.log(fil);

			if (userid) {

				lcModel.setUsers ( users );

				lcModel.setCurrentUser (userid);

				lcModel.chargement = false;

				lcModel.setFil(fil);
				
				lcModel.saveCookie();


			} else {

				console.log('echec de la connexion');
				lcModel.chargement = true;
				lcModel.current_user = undefined;

				//lcVue.showConnectForm();

			}
			
		},

		addLike: function ( questionid ) {
			socket.emit('likequest' , questionid);
		},

		sendMessage: function(msg, type, id) {
			socket.emit('newquest', msg, type, id);
		},

		updateQuest: function(quest) {		
			socket.emit( 'updatequest', JSON.stringify(quest) );
		},

		publishQuest: function(quest) {		
			// A mettre quand la fonction sera faite...
			socket.emit( 'publishquest', JSON.stringify(quest) );
		},

		openchat: function() {
			socket.emit( 'openchat' );
		},

		closechat: function() {
			socket.emit( 'closechat' );
		}

	};

	$().ready( function() {

		livechat.checkCookie();

		socket.on('setuserpropret',function(userid, users, fil) {
			livechat.setuserpropret(userid, JSON.parse(users), JSON.parse(fil));
		});

		socket.on('filupdate', function(modif, msg){

			/*console.log ('---');
			console.log(modif);
			console.log(msg);*/

			if (modif == 'newquest') {
				lcModel.updateMessage(JSON.parse(msg));
			}

			if (modif == 'likequest') {
				lcModel.updateMessage(JSON.parse(msg));
			}

			if (modif == 'updatequest') {

				lcModel.updateMessage( JSON.parse(msg) );
			}

			if (modif == 'publishquest') {
				lcModel.updateMessage( JSON.parse(msg) );
				lcModel.animateMessagesToBottom();
			}

		});

		socket.on('roominfo', function(action, user, livechatinit){

			/*console.log('-- roominfo --');
			console.log(action);
			console.log(user);
			console.log(livechatinit);*/

			if (action == 'openchat') {
				lcModel.setOpenTime(livechatinit);
			} else if (action == 'closechat') {
				lcModel.setOpenTime(livechatinit);
			} else if (action == 'autuser') {
				lcModel.updateUser(JSON.parse(user));
				lcModel.setOpenTime(livechatinit);				
			} else if (action == 'quituser') {
				lcModel.updateUser(JSON.parse(user));
			}		

		});

	});




})( jQuery );






