<?php
/**
 * hm-livechat functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hm-livechat
 */


define('EP_LIVECHAT', 1234);


if ( ! function_exists( 'hm_livechat_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hm_livechat_setup() {


		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		/*register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'hm-livechat' ),
		) );*/

		add_image_size('intervenant', 140, 150, true);
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		/*add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );*/


	}
endif;
add_action( 'after_setup_theme', 'hm_livechat_setup' );





/**
 * Enqueue scripts and styles.
 */
function hm_livechat_scripts() {

	$query_args = array(
		'family' => 'Roboto:300,400'
	);
	wp_enqueue_style( 'hm-livechat-google-fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );

	wp_enqueue_style( 'hm-livechat-style', get_stylesheet_directory_uri() . '/css/theme.css' );
	wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'), false, true );


	global $wp_query;
 
    // if this is not a request for json or a singular object then bail
    if ( isset( $wp_query->query_vars['session'] ) &&  is_singular('livechat') ) {

    	

    	wp_enqueue_script( 'vue-js', get_stylesheet_directory_uri() . '/js/vue.js', array('jquery'), false, true );
    	wp_enqueue_script( 'socket-io', get_stylesheet_directory_uri() . '/js/socket.io.js', array('jquery'), false, true );
    	wp_enqueue_script( 'const-script', get_stylesheet_directory_uri() . '/js/const.js', array('jquery', 'socket-io'), false, true );
    	wp_enqueue_script( 'modele-script', get_stylesheet_directory_uri() . '/js/session-model.js', array('jquery', 'socket-io', 'main-js'), false, true );
        wp_enqueue_script( 'session-script', get_stylesheet_directory_uri() . '/js/session.js', array('jquery', 'main-js', 'socket-io', 'vue-js', 'modele-script', 'const-script'), false, true );



        // Tableau d'avatars

        $avatars = [];

    	while ( have_rows('avatars_publics', 'option') ) : the_row();

			$image_src = wp_get_attachment_image_src( get_sub_field('image'), 'full', false );

			$avatars[get_sub_field('image')] = $image_src[0];

    	endwhile;

    	while ( have_rows('avatars', 'option') ) : the_row();

			$image_src = wp_get_attachment_image_src( get_sub_field('image'), 'full', false );

			$avatars[get_sub_field('image')] = $image_src[0];

    	endwhile;
        
        $data = array(
        	'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
        	'themeurl'	=> get_stylesheet_directory_uri(),
        	'avatars'	=> $avatars
        );

        wp_localize_script( 'session-script', 'data', $data );

    }

}
add_action( 'wp_enqueue_scripts', 'hm_livechat_scripts' );




/**
 * Disable Comments
 */

require_once('inc/disable_comments.php');


/**
 * On nettoie le back-office
 */

function remove_menus(){
  
  
  remove_menu_page( 'edit.php' );                   //Posts
  
}
add_action( 'admin_menu', 'remove_menus' );



/**
 * Function print
 */

function p($s) {
	echo '<pre>';
	print_r ($s);
	echo '</pre>';
}



/**
 * Cache la barre d'admin pour tous
 */

//if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
//}



/**
 * On créé le type LiveChat
 */


function create_livechat() {

	$labels = array(
		'name'                  => 'LiveChats',
		'singular_name'         => 'LiveChat',
		'menu_name'             => 'LiveChat',
		'name_admin_bar'        => 'LiveChat',
		'archives'              => 'Item Archives',
		'attributes'            => 'Item Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'Tous les LiveChats',
		'add_new_item'          => 'Ajouter un nouveau LiveChat',
		'add_new'               => 'Ajouter un nouveau',
		'new_item'              => 'Nouveau LiveChat',
		'edit_item'             => 'Modifier',
		'update_item'           => 'Mettre à jour',
		'view_item'             => 'Voir',
		'view_items'            => 'Voir tous les LiveChats',
		'search_items'          => 'Rechercher',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	
	$rewrite = array(
		'slug'                  => '',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
		'ep_mask'				=> EP_LIVECHAT
	);

	$args = array(
		'label'                 => 'LiveChat',
		'description'           => 'Créé une session de LiveChat',
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-status',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => false,
	);

	register_post_type( 'livechat', $args );

}
add_action( 'init', 'create_livechat', 0 );



/**
 * On créé les URLs
 */

function mhlc_create_endpoints () {
    add_rewrite_endpoint( 'session', EP_LIVECHAT );
}
add_action( 'init', 'mhlc_create_endpoints' );


function mhlc_template_redirect() {
    global $wp_query;
 
    // if this is not a request for json or a singular object then bail
    if ( ! isset( $wp_query->query_vars['session'] ) || ! is_singular('livechat') )
        return;

    add_filter('body_class', 'add_session_to_body');
 
    // include custom template
    include dirname( __FILE__ ) . '/session-template.php';
    exit;
}
add_action( 'template_redirect', 'mhlc_template_redirect' );


function add_session_to_body($classes) {
        $classes[] = 'session';
        return $classes;
}




/*************
 *
 * ACF
 *
 ************/


if( function_exists('acf_add_options_page') ) {

	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Paramètres',
		'menu_title' 	=> 'Paramètres',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

	$avatar_page = acf_add_options_page(array(
		'page_title' 	=> 'Avatars',
		'menu_title' 	=> 'Avatars',
		'menu_slug' 	=> 'avatars',
		'capability' 	=> 'edit_posts',
		'parent_slug' 	=> $option_page['menu_slug'],
		'redirect' 	=> false
	));
	
}



/*************
 *
 * REST API
 *
 ************/


// Gestion des IP

function get_ip_liste( $data ) {

	$array = [];

	if( have_rows('liste_ip', 'option') ):

	 	// loop through the rows of data
	    while ( have_rows('liste_ip', 'option') ) : the_row();

	        $array[] = get_sub_field('adresse_ip');

	    endwhile;

	endif;

 
	return $array;

}

add_action( 'rest_api_init', function () {
  register_rest_route( 'livechat/v1', 'get_ip', array(
    'methods' => 'GET',
    'callback' => 'get_ip_liste',
  ) );
} );





/*************
 *
 * AJAX FROM APP
 *
 ************/


// AJAX Load Chat

add_action( 'wp_ajax_load_chat', 'load_chat' );
add_action( 'wp_ajax_nopriv_load_chat', 'load_chat' );

function load_chat() {

	$uid = $_POST['uid'];
	$pseudo = $_POST['pseudo'];
	$avatar = $_POST['avatar'];
	$status = $_POST['status'];

	//wp_send_json ($_POST);

	$user = array(
		'uid'		=>	$uid,
		'pseudo' 	=>	$pseudo,
		'avatar' 	=>	$avatar,
		'status' 	=>	$status
	);

	// Set Session Cookie
	$hour = 3; // 24 = 24 hours, change number if you want!
 	$calc = 3600 * $hour;

 	setcookie("user_in", json_encode($user), time() + $calc, '/');

 	/*if ($status == 1) {

		get_template_part( 'template-parts/chatroom' );

	} elseif ($status == 2) {

		get_template_part( 'template-parts/chatroom', 'moderateur' );

	}*/

	die();
}


// AJAX Load Chat

/*add_action( 'wp_ajax_load_login', 'load_login' );
add_action( 'wp_ajax_nopriv_load_login', 'load_login' );

function load_login() {

	get_template_part( 'template-parts/chatroom', 'connect-form' );

	die();
}*/



// AJAX Check Cookie

add_action( 'wp_ajax_check_cookie', 'check_cookie' );
add_action( 'wp_ajax_nopriv_check_cookie', 'check_cookie' );

function check_cookie() {

	if(isset($_COOKIE["user_in"])) {
		$user = stripslashes ( $_COOKIE["user_in"] );
		wp_send_json ( json_decode ($user) );
	} else {
		wp_send_json (false);
	}
}



// Crée le bouton aller à la salle pour les admin

function btn_aller_salle( $field ) {
	
    global $post;
    $url = get_permalink($post->ID) . 'session';

    $field['message'] = '<a href="' . $url . '" target="_blank" class="button button-primary">Aller à la salle</a>';

    return $field;
    
}



// key
add_filter('acf/load_field/key=field_5ad5a10fcf1e6', 'btn_aller_salle');




// Check User role


add_action( 'wp_ajax_check_user_status', 'ajax_check_user_status' );
add_action( 'wp_ajax_nopriv_check_user_status', 'ajax_check_user_status' );

function ajax_check_user_status() {

	wp_send_json ( check_user_status() );

}


function check_user_status() {

	$retour = 1;

	if ( is_user_logged_in() ) {

		if ( current_user_can ('delete_posts') ) {
			$retour = 2;
		} else {
			$retour = 3;
		}

	}

	return ($retour);

}






