<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hm-livechat
 */

?>

	</div><!-- #content -->


	<?php
	global $wp_query;

	if ( !isset( $wp_query->query_vars['session'] )): ?>

	<footer id="colophon" class="site-footer py-4">

		<div class="container">

			<div class="row">

				<div class="col">
					<p>HARMONIE MUTUELLE © 2018</p>
				</div>

			</div>

		</div>
		
	</footer><!-- #colophon -->

	<?php endif; ?>


</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
