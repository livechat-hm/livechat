<?php

get_header(); ?>

	<div id="chatroom" class="container p-0">

		<!-- <div v-if=" openTime || current_user.status == 2 || current_user.status == 3 "> -->

			<!-- <div class="row no-gutters" id="header-chat" v-if="current_user.conn && !chargement"> -->

			<div class="row no-gutters d-none d-lg-flex" id="header-chat">
				<div class="col-4 bg-dark pt-5 px-4">
					<div class="media mt-3">
					  <img class="align-self-center mr-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/people.png" alt="">
					  <div class="media-body align-self-center">
					    <p class="text-white m-0"><span class="text-yellow">{{ users_compt }}</span> participants en ligne</p>
					  </div>
					</div>

					<div class="media">
					  <img class="align-self-center mr-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/clock.png" alt="">
					  <div class="media-body align-self-center">
					    <p class="text-white m-0">Le livechat a commencé depuis <span class="text-yellow">{{ time }}</span> min.</p>
					  </div>
					</div>
					
				</div>
				<div class="col-8 pt-4 px-4 align-self-end">



					<h1><?php the_title(); ?></h1>

					<?php if( have_rows('intervenants') ): 

						$texte_intervenants = 'Avec ';
						$interv = [];

						while ( have_rows('intervenants') ) : 

							the_row();
							$interv[] = get_sub_field("nom") . ' <span class="light">(' . get_sub_field("fonction") . ')</span>';

						endwhile;

						$texte_intervenants .= implode(', ', $interv);

						?>
							
						<p class="liste-intervenants"><?php echo $texte_intervenants; ?></p>

				<?php endif; ?>

				</div>
			</div>		


					<?php get_template_part('template-parts/chatroom', 'chargement'); ?>

		<?php get_template_part( 'template-parts/chatroom', 'connect-form' ); ?>

			<div class="row no-gutters">

				<div class="col-4">
					
					<?php /* MODERATEUR */ ?>

					<?php get_template_part( 'template-parts/questions', 'moderateur' ); ?>

					<?php /* REDACTEUR */ ?>

					<?php get_template_part( 'template-parts/questions', 'redacteur' ); ?>

					<?php /* PARTICIPANT */ ?>

					<?php get_template_part( 'template-parts/questions', 'participant' ); ?>


					<?php /* MODERATEUR + REDACTEUR */ ?>

					<?php get_template_part( 'template-parts/single-question', 'edit' ); ?>
					<?php get_template_part( 'template-parts/single-reponse', 'edit' ); ?>

				</div>

				<div class="col-8">
					
					<?php get_template_part( 'template-parts/fil' ); ?>
				</div>

			</div>


		<?php /*<div v-else>
			
			<?php get_template_part( 'template-parts/en-attente' ); ?>

		</div> */ ?>

	</div>
		
		<?php //get_template_part('template-parts/chatroom', 'chargement'); ?>

<?php
get_footer();
