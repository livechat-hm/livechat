<div v-if="current_user.conn && !chargement">

	<div id="messages">
		<div class="publication-container">
			<div class="inner">
				<div class="question" v-for="question in publication">
					<?php get_template_part( 'template-parts/single-question', 'fil' ); ?>
					<?php //get_template_part( 'template-parts/single-question', 'participant' ); ?>
				</div>
			</div>
		</div>	
	</div>

	<form class="" id="send-form"  v-on:submit.prevent="sendMessage">
		<div class="row">
		   <div class="col-lg-12">
		    <div class="input-group input-group-lg">
		      <input type="text" class="form-control input-lg" id="chat-zone" placeholder="Votre question">
		      <span class="input-group-btn">
		        <button class="btn btn-envoyer mb-0 btn-lg" type="submit">Envoyer</button>
		      </span>
		    </div>
		  </div>
		</div>
		<!-- <div class="form-group mb-2">
			<label for="chat-zone" class="sr-only">Input</label>
			<input class="form-control form-control-lg" type="text" id="chat-zone" placeholder="Votre question">
		</div>
		<button type="submit" class="btn btn-primary mb-0 btn-lg">Envoyer</button> -->
	</form>

</div>