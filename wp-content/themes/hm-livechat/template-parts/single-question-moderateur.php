<div v-if="current_user.status != 1">

	<div class="media">
	  <img v-bind:src="question.user | getAvatarByUser" class="img-fluid avatar mr-3">
	  <div class="media-body">
	    <h5 class="">{{ question.user | getNom }}</h5>
	    <p class="keep-lines">{{ question.question | messagePrint }}</p>
	  </div>
	</div>

	<div class="controller">
		<button class="btn-refuse btn btn-link" v-on:click="refuse(question.id)" v-if="question.status != 3"><i class="fa fa-times-circle"></i></button>
		<button class="btn-refuse btn btn-link" v-on:click="restaurer(question.id)" v-if="question.status == 3"><i class="fa fa-undo"></i></button>
		<star-rating v-bind:increment="1" 
		             v-bind:max-rating="5" 
		             inactive-color="#eee" 
		             active-color="#bbbe07" 
		             v-bind:star-size="15"
		             v-bind:show-rating="false"
		             v-bind:rating="question.note"
		             v-bind:inline="true"
		             @rating-selected ="setRating($event, question.id)">            	
		</star-rating>
		
		<button type="button" class="btn btn-primary btn-valider btn-sm mx-3" v-on:click="validerQuest(question.id)" v-if="question.status != 3 && question.status != 1 && question.status != 4"><span v-if="question.status == 0">Valider</span><span v-if="question.status == 2">Publier</span></button>
		<button type="button" class="btn btn-primary btn-repondre btn-sm mx-3" data-toggle="modal" data-target="#edit-reponse" v-on:click="editReponse(question)" v-if="question.status == 1">Répondre</button>
		<button type="button" class="btn btn-primary btn-reponse btn-sm mx-3" v-on:click="publierReponse(question.repid)" v-if="question.status == 4 && question.type == 0">Publier la réponse</button>

		<button type="button" class="btn btn-primary btn-like btn-sm" v-on:click="addLike(question.id)" v-bind:class="{ disabled: checkLike(question.id) }">Like </button><span class="badge badge-light like-compte">{{ question.like.length }}</span>
	</div>

	<div class="dropdown question-more">
	  <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
	  </button>
	  <div class="dropdown-menu">
	    <button type="button" class="dropdown-item" data-toggle="modal" data-target="#edit-question" v-on:click="editQuestion(question)">
		  Modifier
		</button>
		<button type="button" class="dropdown-item" data-toggle="modal" v-if="question.status == 1 || question.status == 2" data-target="#edit-reponse" v-on:click="editReponse(question)">
		  Répondre
		</button>
	  </div>
	</div>

</div>