<div id="questions-moderateur" v-if="current_user.status==='2' && !chargement">

	<ul class="nav nav-tabs" id="questions-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="nouvelles-tab" data-toggle="tab" href="#nouvelles" role="tab" aria-controls="nouvelles" aria-selected="true">Nouvelles <span class="badge badge-secondary">{{ nouvelles_questions.length }}</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="en-cours-tab" data-toggle="tab" href="#en-cours" role="tab" aria-controls="en-cours" aria-selected="false">À rédiger <span class="badge badge-secondary">{{ rediger_questions.length }}</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="a-publier-tab" data-toggle="tab" href="#a-publier" role="tab" aria-controls="a-publier" aria-selected="false">À publier <span class="badge badge-secondary">{{ a_publier_questions.length }}</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="refusees-tab" data-toggle="tab" href="#refusees" role="tab" aria-controls="refusees" aria-selected="false">Refusées <span class="badge badge-secondary">{{ refusees_questions.length }}</span></a>
		</li>
	</ul>

	<div class="tab-content" id="questions-content">
		<div class="tab-pane fade show active" id="nouvelles" role="tabpanel" aria-labelledby="nouvelles-tab">
			<div class="questions-container">
				<div class="question" v-for="question in nouvelles_questions">
					<?php get_template_part( 'template-parts/single-question', 'moderateur' ); ?>
				</div>
			</div>			
		</div>
		<div class="tab-pane fade" id="a-publier" role="tabpanel" aria-labelledby="a-publier-tab">
			<div class="questions-container">
				<div class="question" v-for="question in a_publier_questions">
					<?php get_template_part( 'template-parts/single-question', 'moderateur' ); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="en-cours" role="tabpanel" aria-labelledby="en-cours-tab">
			<div class="questions-container">
				<div class="question" v-for="question in rediger_questions">
					<?php get_template_part( 'template-parts/single-question', 'moderateur' ); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="refusees" role="tabpanel" aria-labelledby="refusees-tab">
			<div class="tab-pane fade show active" id="nouvelles" role="tabpanel" aria-labelledby="nouvelles-tab">
				<div class="questions-container">
					<div class="question" v-for="question in refusees_questions">
						<?php get_template_part( 'template-parts/single-question', 'moderateur' ); ?>
					</div>
				</div>			
			</div>
		</div>
	</div>

	<div id="openclose">
		<button class="btn-primary btn btn-primary" v-if="!openTime" v-on:click="openchat()"><i class="fa fa-play-circle"></i></button>
		<button class="btn-primary btn btn-primary" v-if="openTime" v-on:click="closechat()"><i class="fa fa-stop-circle"></i></button>
	</div>

</div>


<?php get_template_part( 'template-parts/single-question', 'edit' ); ?>


