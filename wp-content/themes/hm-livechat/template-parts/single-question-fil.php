<!-- <div>
	<div class="media">
	  <img v-bind:src="question.user | getAvatarByUser" class="img-fluid avatar mr-3 rounded-circle">
	  <div class="media-body">
	    <h5 class="mt-0">{{ question.user | getNom }}</h5>
	    <p class="keep-lines">{{ question.question | messagePrint }}</p>
	  </div>
	</div>

	<div class="controller">
		<button type="button" class="btn btn-primary btn-like" v-bind:class="{ disabled: checkLike(question.id) }" v-on:click="addLike(question.id)">Like <span class="badge badge-light like-compte">{{ question.like.length }}</span></button>
	</div>

</div> -->

<div class="question-fil p-2">
	<div class="row" v-bind:class="{ 'justify-content-end': question.type }">
		<div class="col-8">
			<div class="row">
				<div class="col-2">
					<img v-bind:src="question.user | getAvatarByUser" class="img-fluid avatar mr-3 d-block mx-auto">
				</div>
				<div class="col-10" v-bind:class="{ 'order-first': question.type }">
					<div class="alert" v-bind:class="{ 'alert-primary': question.type, 'alert-secondary': !question.type }">
						<p class="heading-question-fil"><span class="pseudo">{{ question.user | getNom }}</span><span class="date-publi">{{ question.publication | printHour }}</span></h5>
						<p class="keep-lines">{{ question.question | messagePrint }}</p>
						<div class="controller text-right">
							<button type="button" class="btn btn-primary btn-reponse" v-on:click="publierReponse(question.repid, question)" v-if="question.status == 4 && question.type == 0 && current_user.status == 2">Publier la réponse</button>
							<button type="button" class="btn btn-primary btn-like" v-bind:class="{ disabled: checkLike(question.id) }" v-on:click="addLike(question.id)">Like</button><span class="badge badge-light like-compte">{{ question.like.length }}</span>
						</div>
					</div>

				</div>
			</div>
	    </div>
	</div>
</div>