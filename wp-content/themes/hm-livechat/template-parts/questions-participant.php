<div id="questions-moderateur" v-if="current_user.status==='1' && !chargement">

	<ul class="nav nav-tabs" id="questions-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#nouvelles" role="tab" aria-controls="avenir" aria-selected="true">Questions à venir <span class="badge badge-secondary">{{ a_venir_questions.length }}</span></a>
		</li>
	</ul>

	<div class="tab-content" id="questions-content">
		<div class="tab-pane fade show active" id="avenir" role="tabpanel" aria-labelledby="avenir">
			<div class="questions-container">
				<div class="question" v-for="question in a_venir_questions">
					<?php get_template_part( 'template-parts/single-question', 'participant' ); ?>
				</div>
			</div>			
		</div>
	</div>

</div>


