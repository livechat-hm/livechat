<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container">

		<div class="row">
			
			<div class="col-lg-8 col-contenu pb-5">

				<?php if( have_rows('carousel') ): ?>

					<div class="carousel">

				    <?php while ( have_rows('carousel') ) : the_row(); ?>

				    	<div class="slide">
				    		<?php echo wp_get_attachment_image( get_sub_field("image"), "large", false, array( "class" => "img-fluid") ); ?>
				    		<div class="slide-content">
								<?php if (get_sub_field("sous-titre")): ?>
									<h4><?php the_sub_field("sous-titre"); ?></h4>
								<?php endif; ?>
								<?php if (get_sub_field("titre")): ?>
									<h3><?php the_sub_field("titre"); ?></h3>
								<?php endif; ?>
				    		</div>
				       	</div>

				    <?php endwhile; ?>
					
					</div>

				<?php endif; ?>

				<div class="inner">

					<h2><?php the_field("titre"); ?></h2>

					<p class="lead"><?php the_field("chapo"); ?></p>

					<?php if (get_field("punchline")): ?>

						<div class="punchline">
							<div class="media">

					    		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/homme-bulle.gif" alt="" class="mr-5">
				    			<div class="media-body align-self-center">
									<h2><?php the_field("punchline"); ?></h2>
				    			</div>
				       		</div>
						</div>

					<?php endif; ?>

					<?php the_field("contenu"); ?>

				</div>

			</div>

			<div class="col-lg-4 sidebar">
				
				<?php if( have_rows('intervenants') ): ?>			

					<div class="intervenants side-block primary">

						<div class="intervenants-carousel">

						<?php while ( have_rows('intervenants') ) : the_row(); ?>

						    	<div class="intervenant">

						    		<div class="media">

							    		<?php echo wp_get_attachment_image( get_sub_field("photo"), "intervenant", false, array( "class" => "align-self-center mr-3 portrait") ); ?>
						    			<div class="media-body align-self-center">
						    				<p>avec</p>
											<?php if (get_sub_field("nom")): ?>
												<h5><?php the_sub_field("nom"); ?></h5>
											<?php endif; ?>
											<?php if (get_sub_field("fonction")): ?>
												 <p class="mb-0"><?php the_sub_field("fonction"); ?></p>
											<?php endif; ?>
						    			</div>
						       		</div>

						       	</div>

						    <?php endwhile; ?>
							
							</div>

					</div>

					<svg width="0" height="0" class="masque">
						<defs>
					    	<clipPath id="myClip">
								<path d="M121.7,125.5l-34.3,19.8c-10.9,6.3-24.4,6.3-35.4,0l-34.3-19.8C6.7,119.1,0,107.4,0,94.8V55.2c0-12.6,6.7-24.3,17.7-30.6
						L52,4.7c10.9-6.3,24.4-6.3,35.4,0l34.3,19.8c10.9,6.3,17.7,18,17.7,30.6v39.6C139.4,107.4,132.6,119.1,121.7,125.5z"/>
							</clipPath>
						</defs>
					</svg>

				<?php endif; ?>


				<div class="question side-block primary">

					<h2 class="mb-3">Poser une question</h2>

					<form>
					 	<div class="form-group">
					    	<input type="text" required class="form-control" id="pseudo" placeholder="Pseudo...">
						</div>
						<div class="form-group">
					    	<textarea required class="form-control" id="question" rows="3" placeholder="Posez votre question..."></textarea>
					  	</div>
					  	<div class="text-center">
					  		<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#questionsModal">Lire les questions déjà posées</button>
					  		<button type="submit" class="btn btn-primary">Envoyer</button>
					  	</div>
					</form>

				</div>


				<div class="compteur side-block yellow">

					<div class="before">

						<h2 class="mb-3">Le live démarre dans</h2>

						<p id="date" class="d-none"><?php the_field('date_et_heure'); ?></p>

						<div class="row no-gutters text-center justify-content-center">

							<div class="col-2 col-time">

								<div id="jours">07</div>

								<span>JOURS</span>

							</div>

							<div class="col-1 col-points">:</div>

							<div class="col-2 col-time">

								<div id="heures">02</div>

								<span>HEURES</span>

							</div>

							<div class="col-1 col-points">:</div>

							<div class="col-2 col-time">

								<div id="minutes">48</div>

								<span>MINUTES</span>

							</div>

							<div class="col-1 col-points">:</div>

							<div class="col-2 col-time">

								<div id="secondes">03</div>

								<span>SECONDES</span>

							</div>

						</div>

					</div>

					<div class="during d-none text-center">

						<h2 class="mb-3">Le live est démarré</h2>

						<a class="btn btn-secondary" href="<?php the_permalink(); ?>/session">Entrer dans la chat room</a>


					</div>

				</div>

			</div>

		</div>

	</div>

</article>


<!-- Modal -->
<div class="modal fade" id="questionsModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Les questions déjà posées</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>