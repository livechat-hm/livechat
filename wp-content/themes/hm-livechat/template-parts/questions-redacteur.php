<div id="questions-moderateur" v-if="current_user.status==='3' && !chargement">

	<ul class="nav nav-tabs" id="questions-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link" id="en-cours-tab" data-toggle="tab" href="#en-cours" role="tab" aria-controls="en-cours" aria-selected="false">À rédiger</a>
		</li>
	</ul>

	<div class="tab-content" id="questions-content">

		<div class="tab-pane fade show active" id="en-cours" role="tabpanel" aria-labelledby="en-cours-tab">
			<div class="questions-container">
				<div class="question" v-for="question in rediger_questions">
					<?php get_template_part( 'template-parts/single-question', 'moderateur' ); ?>
				</div>
			</div>
		</div>
		
	</div>

</div>

