<div class="modal" tabindex="-1" role="dialog" id="edit-question">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modifier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form v-on:submit.prevent="editQuest">
          <div class="form-group">
            <label for="message">Message</label>
            <textarea class="form-control" id="message" rows="5" v-model="modalQuestion.question">{{ modalQuestion.question }}</textarea>
          </div>
          <input type="hidden" id="id">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Annuler</button>
          <button type="submit" class="btn btn-primary">Valider</button>
        </form>
      </div>
    </div>
  </div>
</div>