<div v-if="current_user.status == 1">

	<div class="media">
	  <img v-bind:src="question.user | getAvatarByUser" class="img-fluid avatar mr-3">
	  <div class="media-body">
	    <h5 class="">{{ question.user | getNom }}</h5>
	    <p class="keep-lines">{{ question.question | messagePrint }}</p>
	  </div>
	</div>

	<div class="controller">
		<button type="button" class="btn btn-primary btn-like" v-bind:class="{ disabled: checkLike(question.id) }" v-on:click="addLike(question.id)">Like <span class="badge badge-light like-compte">{{ question.like.length }}</span></button>
	</div>

</div>