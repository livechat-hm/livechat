<div class="row justify-content-center my-5" v-if="connection_needed">

	<div class="col-6">

		<form id="connect-form" v-on:submit.prevent="connectForm">

			<?php


			$status = check_user_status();

			if ( $status == 2 || $status == 3 ):

				if ( $status == 2 ) {
					$texte = 'modérateur';
				} else {
					$texte = 'rédacteur';
				}

				?>

				<div class="alert alert-primary" role="alert">
					Vous êtes connecté en tant que <?php echo $texte; ?>.
				</div>

			<?php endif; ?>
							
			<div class="form-group">
				<label for="pseudo">Pseudo</label>
				<input type="text" class="form-control" id="pseudo" placeholder="Choisissez votre pseudo..." required>
				</div>

			<div class="form-group avatar-select">
				<label for="avatar">Choisissez votre avatar</label>
			    <select class="form-control" id="avatar">
			    	<?php while ( have_rows('avatars_publics', 'option') ) : the_row();

						$image_src = wp_get_attachment_image_src( get_sub_field('image'), 'full', false ); ?>

			        	<option data-img-src="<?php echo $image_src[0]; ?>" value="<?php the_sub_field('image'); ?>"> X </option>

			    	<?php endwhile; ?>
					

					<?php if ( $status == 2 || $status == 3 ):

						while ( have_rows('avatars', 'option') ) : the_row();

							$image_src = wp_get_attachment_image_src( get_sub_field('image'), 'full', false ); ?>

				        	<option data-img-src="<?php echo $image_src[0]; ?>" value="<?php the_sub_field('image'); ?>"> X </option>

				    	<?php endwhile;

					endif; ?>

			    </select>
				

			</div>

			<button type="submit" class="btn btn-primary">Rejoindre le chat</button>

		</form>

	</div>

</div>
