<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'hm_livechat');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%~d9}}&EvfAQ5YVmz=9T]DP7g&~ ZlaDh3AB00fex(Ds% E%<G B`5u]@{e,ZT^~');
define('SECURE_AUTH_KEY',  'kf9Iw4PHp2MB9Dp0n6q9$U,;.ghdE&h,i<R-4K_rX>AW>[]%/+>!o+;M3|H#nJ:N');
define('LOGGED_IN_KEY',    '6$}`+SyE{=`Fb.ds!#>fnB,AikA.Wx8s{V)xzrw@Pj3KndV?Z}2b-#S+]kBM(6XP');
define('NONCE_KEY',        'WvlCvIO:%/)_^,i^eCiR]8!/=qYh[y[5>eIe9aw/AuNfh*azD[NGfZ?p.zvr_f6P');
define('AUTH_SALT',        '@PEzzcQ!I;YAq;L**CgjR>;_zw,#wwz:~YV?x-}.+sUeu4V)c{|+ubM@zo>2,fU-');
define('SECURE_AUTH_SALT', '*nz>LqN/oV&cp-`)q+vOQ,]z$UrK)TTc:!F7|t9w@*DI0!Wm#x2:DC*KFESYq @l');
define('LOGGED_IN_SALT',   ']TyiAp)MK<#H/hwQm yxbmNR__swl:Nm@x==a$1R|-H]P1C[9z8&uR6mQR*tEdNY');
define('NONCE_SALT',       'lh3n|DzC]Wx#/W,1E-) qHPZtEgtjM/42ctG6ErG{L1kr6ixR+FwmZDgH[g@k6&h');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');